/**
 * \file engine.cpp
 * \brief Engine.
 *
 * \author Matteo Foppiano <matteo@redsplat.org>
 * \date 18 Apr 2014
 */

#include <cassert>
#include <rs/engine.h>


/***************************************************************************//**
 * \brief Constructor.
 *
 */

engine::engine(const std::string& name)
	:
	my_name(name),
	gametick(0),
	tot_laps(0),
	path_eval_brick(0),
	main_fsm(EVAL_ROUTE),
	racing_trk(),
	players(),
	my_car(NULL),
	msg_fifo()
{
}

/***************************************************************************//**
 * \brief Initialize the track.
 *
 * \param j_track "track" json message.
 *
 */

void engine::set_track(const jsoncons::json& j_track)
{
	this->racing_trk.set_id(j_track["id"].as<std::string>());
	this->racing_trk.set_name(j_track["name"].as<std::string>());

	for (size_t i=0; i<j_track["lanes"].size(); ++i) {
		this->racing_trk.add_lane(lane(
		    j_track["lanes"][i]["index"].as<int>(),
		    j_track["lanes"][i]["distanceFromCenter"].as<double>()));
	}

	for (size_t i=0; i<j_track["pieces"].size(); ++i) {
		this->racing_trk.add_brick(track_piece(
		    j_track["pieces"][i].has_member("length") ?
		    j_track["pieces"][i]["length"].as<double>() : 0.0,
		    j_track["pieces"][i].has_member("radius") ?
		    j_track["pieces"][i]["radius"].as<double>() : 0.0,
		    j_track["pieces"][i].has_member("angle") ?
		    j_track["pieces"][i]["angle"].as<double>() : 0.0,
		    j_track["pieces"][i].has_member("switch") ?
		    j_track["pieces"][i]["switch"].as<std::string>() == "true" :
		    false));
	}

	this->racing_trk.build_track_lengths();
}

/***************************************************************************//**
 * \brief Parse json messages and initialize the cars.
 *
 * \param j_cars "cars" json message.
 *
 */

void engine::set_cars(const jsoncons::json& j_cars)
{
	for (size_t i=0; i<j_cars.size(); ++i) {
		this->players.add_car(car(
		    j_cars[i]["id"]["name"].as<std::string>(),
		    j_cars[i]["id"]["color"].as<std::string>(),
		    j_cars[i]["dimensions"]["length"].as<double>(),
		    j_cars[i]["dimensions"]["width"].as<double>(),
		    j_cars[i]["dimensions"]["guideFlagPosition"].as<double>()));
		if (j_cars[i]["id"]["name"].as<std::string>() == my_name) {
			const std::string& cc =
			    j_cars[i]["id"]["color"].as<std::string>();
			assert(this->players.is_a_racer(cc));
			if (this->players.is_a_racer(cc))
				this->my_car = &this->players.get_racer(cc);
		}
	}
}

/***************************************************************************//**
 * \brief Update the cars' status.
 *
 * \param j_cars json cars status.
 *
 */

void engine::update_cars(const jsoncons::json& j_cars)
{
	for (size_t i=0; i<j_cars.size(); ++i) {
		auto& j_car = j_cars[i];
		const std::string& cc =
		    j_car["id"]["color"].as<std::string>();

		assert(this->players.is_a_racer(cc));
		if (!this->players.is_a_racer(cc)) {
			continue;
		}

		car& c = this->players.get_racer(cc);

		c.update_angle(j_car["angle"].as<double>());
		c.update_lane(
		    j_car["piecePosition"]["lane"]["startLaneIndex"].as<int>(),
		    j_car["piecePosition"]["lane"]["endLaneIndex"].as<int>());
		c.update_lap(j_car["piecePosition"]["lap"].as<int>());
		int b_idx = j_car["piecePosition"]["pieceIndex"].as<int>();
		double l_len = this->racing_trk.get_lane_length(
		    b_idx, c.get_cur_lane(), c.get_next_lane());
		c.update_brick(
		    b_idx,
		    j_car["piecePosition"]["inPieceDistance"].as<double>(),
		    this->racing_trk.get_brick_radius(b_idx),
		    l_len);

		c.update_status(this->gametick);
	}
}

/***************************************************************************//**
 * \brief Set a car status to inactive.
 *
 * \param j_car json crash message.
 *
 */

void engine::car_crashed(const jsoncons::json& j_car)
{
	const std::string& cc = j_car["color"].as<std::string>();
	assert(this->players.is_a_racer(cc));
	if (!this->players.is_a_racer(cc)) {
		return;
	}
	this->players.get_racer(cc).set_status(car::CAR_S_WAITING);
}

/***************************************************************************//**
 * \brief Set a car status to active.
 *
 * \param j_car json spawn message.
 *
 */

void engine::car_spawn(const jsoncons::json& j_car)
{
	const std::string& cc = j_car["color"].as<std::string>();
	assert(this->players.is_a_racer(cc));
	if (!this->players.is_a_racer(cc)) {
		return;
	}
	this->players.get_racer(cc).set_status(car::CAR_S_RACING);
}

/***************************************************************************//**
 * \brief Let's the game begin.
 *
 */

void engine::game_start(void)
{
	this->players.start_all_engines();
}

/***************************************************************************//**
 * \brief Evaluate next move.
 *
 */

void engine::eval(void)
{
	switch (this->main_fsm) {
	case EVAL_ROUTE:
		eval_track_route();
		break;
	default:
	case EVAL_THROTTLE:
		eval_car_throttle();
		break;
	}

	evolve_fsm();
}

/***************************************************************************//**
 * \brief Evolve FSMs into next state.
 *
 */

void engine::evolve_fsm(void)
{
	switch (this->main_fsm) {
	case EVAL_ROUTE:
		this->main_fsm = EVAL_THROTTLE;
		break;
	default:
	case EVAL_THROTTLE:
		assert(this->my_car != NULL);
		if (this->my_car != NULL &&
		    this->my_car->get_cur_brick_idx() ==
		    this->path_eval_brick) {
			this->main_fsm = EVAL_ROUTE;
		}
		break;
	}
}

/***************************************************************************//**
 * \brief Evaluate track route (lane switching).
 *
 */

void engine::eval_track_route(void)
{
	assert(this->my_car != NULL);
	if (this->my_car == NULL) {
		return;
	}

	path_lane path;
	int n_laps = this->tot_laps > 0 ?
	    this->tot_laps - this->my_car->get_lap() : 1;
	const path_length& cur_path = this->racing_trk.get_shortest_path(
	    this->my_car->get_cur_brick_idx(), this->my_car->get_cur_lane(),
	    n_laps, path);

	if (!cur_path.sw_list.empty()) {
		const switch_info& sw = cur_path.sw_list.at(0);
		if (this->racing_trk.get_switch_after(
		    this->my_car->get_cur_brick_idx()) == sw.b_idx) {
			set_switch_lane(sw.type);
			this->path_eval_brick =
			    this->racing_trk.get_next_brick_idx(sw.b_idx);
		} else {
			this->path_eval_brick =
			    this->racing_trk.get_switch_before(sw.b_idx);
		}
	} else {
		this->path_eval_brick = 0;
	}
}

/***************************************************************************//**
 * \brief Evaluate car throttle.
 *
 */

void engine::eval_car_throttle(void)
{
	assert(this->my_car != NULL);
	if (this->my_car == NULL) {
		return;
	}

	this->racing_trk.update_stats(
	    this->my_car->get_cur_brick_idx(),
	    this->my_car->get_cur_brick_pos(),
	    this->my_car->get_cur_lane(),
	    this->my_car->get_throttle(),
	    this->my_car->get_angle());

	set_throttle(this->my_car->get_throttle());
}

/***************************************************************************//**
 * \brief Set switch lane.
 *
 * \param dir direction to switch to.
 *
 */

void engine::set_switch_lane(switch_info::sw_type dir)
{
	this->msg_fifo.push(hwo_protocol::make_switch(dir));
}

/***************************************************************************//**
 * \brief Set throttle value.
 *
 * \param th new throttle value.
 *
 */

void engine::set_throttle(double th)
{
	assert(th >= car::MIN_THROTTLE);
	assert(th <= car::MAX_THROTTLE);

	this->msg_fifo.push(hwo_protocol::make_throttle(th));
}
