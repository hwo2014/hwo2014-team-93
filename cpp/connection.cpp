#include <rs/connection.h>


hwo_connection::hwo_connection(const std::string& host, const std::string& port)
	:
	io_service(),
	socket(this->io_service),
	response_buf()
{
	boost::asio::ip::tcp::resolver resolver(this->io_service);
	boost::asio::ip::tcp::resolver::query query(host, port);
	boost::asio::connect(this->socket, resolver.resolve(query));
	this->response_buf.prepare(8192);
}

hwo_connection::~hwo_connection(void)
{
	this->socket.close();
}

jsoncons::json hwo_connection::receive_response(
    boost::system::error_code& error)
{
	auto len = boost::asio::read_until(
	    this->socket, this->response_buf, "\n", error);
	if (error) {
		return jsoncons::json();
	}
	auto buf = this->response_buf.data();
	std::string reply(
	    boost::asio::buffers_begin(buf),
	    boost::asio::buffers_begin(buf) + len);
	this->response_buf.consume(len);
	return jsoncons::json::parse_string(reply);
}

void hwo_connection::send_requests(const std::vector<jsoncons::json>& msgs)
{
	jsoncons::output_format format;
	format.escape_all_non_ascii(true);
	boost::asio::streambuf request_buf;
	std::ostream s(&request_buf);

	for (const auto& m : msgs) {
		m.to_stream(s, format);
		s << std::endl;
	}
	this->socket.send(request_buf.data());
}
