#include <rs/game_logic.h>
#include <rs/protocol.h>


using namespace hwo_protocol;

game_logic::game_logic(const std::string& name)
	:
	action_map {
		{ "join", &game_logic::on_join },
		{ "joinRace", &game_logic::on_join_race },
		{ "yourCar", &game_logic::on_your_car },
		{ "gameInit", &game_logic::on_game_init },
		{ "gameStart", &game_logic::on_game_start },
		{ "carPositions", &game_logic::on_car_positions },
		{ "crash", &game_logic::on_crash },
		{ "lapFinished", &game_logic::on_lap_finished },
		{ "finish", &game_logic::on_finish },
		{ "gameEnd", &game_logic::on_game_end },
		{ "tournamentEnd", &game_logic::on_tournament_end },
		{ "error", &game_logic::on_error },
		{ "turboAvailable", &game_logic::on_turbo },
		{ "spawn", &game_logic::on_spawn },
		{ "dnf", &game_logic::on_dnf },
	},
	driveshaft(name)
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	bool sync_msg = false;
	if (msg.has_member("gameTick")) {
		this->driveshaft.update_gametick(msg["gameTick"].as<int>());
		sync_msg = true;
	}
	auto action_it = this->action_map.find(msg_type);

	bool send_reply = false;
	if (action_it != this->action_map.end()) {
		send_reply = (action_it->second)(this, data, sync_msg);
	} else {
		std::cout << "unknown message '" << msg_type << "' (" <<
		    sync_msg << "): " << data << std::endl;
		send_reply = sync_msg;
	}

	if (send_reply) {
		return { this->driveshaft.msg_pop() };
	}
	return { };
}

bool game_logic::on_join(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
	(void) sync;
#endif
	return (false);
}

bool game_logic::on_join_race(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
	(void) sync;
#endif
	return (false);
}

bool game_logic::on_your_car(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
	(void) sync;
#endif
	return (false);
}

bool game_logic::on_game_init(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#endif
	this->driveshaft.set_track(data["race"]["track"]);
	this->driveshaft.set_cars(data["race"]["cars"]);
	if (data["race"]["raceSession"].has_member("laps")) {
		this->driveshaft.set_tot_laps(
		    data["race"]["raceSession"]["laps"].as<int>());
	}
	return (sync);
}

bool game_logic::on_game_start(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
	(void) sync;
#endif
	this->driveshaft.game_start();
	this->driveshaft.eval();
	return (true);
}

bool game_logic::on_car_positions(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#endif
	this->driveshaft.update_cars(data);
	if (sync) {
		this->driveshaft.eval();
	}

	return (sync);
}

bool game_logic::on_crash(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) sync;
#endif
	this->driveshaft.car_crashed(data);
	return (false);
}

bool game_logic::on_lap_finished(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) sync;
	(void) data;
#endif
	return (false);
}

bool game_logic::on_finish(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
#endif
	return (sync);
}

bool game_logic::on_game_end(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
	(void) sync;
#endif
	this->driveshaft.clear_all();
	return (false);
}

bool game_logic::on_tournament_end(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
	(void) sync;
#endif
	return (false);
}

bool game_logic::on_error(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
#endif
	return (sync);
}

bool game_logic::on_turbo(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
#endif
	return (sync);
}

bool game_logic::on_spawn(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#endif
	this->driveshaft.car_spawn(data);
	return (sync);
}

bool game_logic::on_dnf(const jsoncons::json& data, bool sync)
{
#if 0
	std::cout << __func__ << "(" << sync << "): " << data << std::endl;
#else
	(void) data;
#endif
	return (sync);
}
