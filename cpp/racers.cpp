/**
 * \file racers.cpp
 * \brief Racers management.
 *
 * \author Matteo Foppiano <matteo@redsplat.org>
 * \date 18 Apr 2014
 */


#include <cassert>
#include <cmath>
#include <rs/racers.h>


/************************************************************************ Racers
 */

/***************************************************************************//**
 * \brief Racers constructor.
 *
 */

racers::racers(void)
	:
	racer()
{
}

/***************************************************************************//**
 * \brief Add a racer to the crowd.
 *
 * \param c car to add.
 *
 */

void racers::add_car(const car& c)
{
	this->racer.insert(
	    std::pair<std::string, car>(c.get_color(), c));
}

/***************************************************************************//**
 * \brief Check that a racer is present.
 *
 * \param color color of the car's racer to check.
 * \return true if the racer is present.
 *
 */

bool racers::is_a_racer(const std::string& color) const
{
	if (this->racer.find(color) != this->racer.end()) {
		return (true);
	}
	return (false);
}

/***************************************************************************//**
 * \brief Find a racer in the list.
 *
 * \param color color of the car's racer to find.
 * \return found car.
 *
 */

car& racers::get_racer(const std::string& color)
{
	return (this->racer.find(color)->second);
}

/***************************************************************************//**
 * \brief Enable all engines.
 *
 */

void racers::start_all_engines(void)
{
	for (std::map<std::string, car>::iterator r = this->racer.begin();
	    r != this->racer.end();
	    ++r) {
		r->second.set_status(car::CAR_S_RACING);
	}
}


/*************************************************************************** Car
 */

/***************************************************************************//**
 * \brief Car constructor.
 *
 * \param n name of the player
 * \param c color of the car's player.
 * \param l length of the car.
 * \param w width of the car.
 * \param gf position of the guide flag.
 *
 */

car::car(const std::string& n, const std::string& c,
    double l, double w, double gf)
	:
	name(n),
	color(c),
	length(l),
	width(w),
	guide_flag(gf),
	inertia(
	    (this->width / 3) *
	    (pow(this->length-this->guide_flag, 3) +
	    pow(this->guide_flag, 3))),
	angle(0.0),
	cur_brick_idx(0),
	cur_brick_pos(0.0),
	cur_brick_rad(0.0),
	cur_lane(UNDEF_LANE),
	next_lane(UNDEF_LANE),
	cur_lane_len(0.0),
	lap(0),
	throttle(0.5),
	prev_brick_idx(0),
	prev_brick_pos(0.0),
	prev_brick_rad(0.0),
	prev_lane_len(0.0),
	l_speed(0.0),
	w_speed(0.0),
	l_acc(0.0),
	w_acc(0.0),
	fi(0.0),
	slip_rad(0.0),
	slip_speed(0.0),
	slip_acc(0.0),
	gametick(0),
	physic_fsm(CAR_E_PHYSIC),
	state_fsm(CAR_S_WAITING),
	e_power(0),
	drag(0)
{
}

/***************************************************************************//**
 * \brief Update the status of the car.
 *
 * \param t current game tick.
 *
 */

void car::update_status(int t)
{
	int dt = t - this->gametick;
	if (dt == 0) {
		return;
	}
	this->gametick = t;

	double dl = 0.0;
	if (this->cur_brick_idx == this->prev_brick_idx) {
		dl = this->cur_brick_pos - this->prev_brick_pos;
	} else {
		dl = this->prev_lane_len - this->prev_brick_pos +
		    this->cur_brick_pos;
	}
	double prev_l_speed = this->l_speed;
	this->l_speed = dl / dt;

	double prev_w_speed = this->w_speed;
	if (this->cur_brick_rad > 0.0) {
		this->w_speed = this->l_speed / this->cur_brick_rad;
	} else {
		this->w_speed = 0.0;
	}
	this->fi = this->w_speed * this->w_speed * this->cur_brick_rad *
	    this->length * this->width;

	double prev_l_acc = this->l_acc;
	this->l_acc = (this->l_speed - prev_l_speed) / dt;
	this->w_acc = (this->w_speed - prev_w_speed) / dt;

	if (this->physic_fsm == CAR_E_PHYSIC) {
		if (prev_l_acc > 0.0) {
			this->e_power = this->throttle / prev_l_acc;
			this->drag = (this->l_acc - prev_l_acc) / prev_l_acc;
			this->physic_fsm = CAR_E_DONE;
		}
	}

	this->slip_acc =
	    (((this->length / 2.0) - this->guide_flag) * this->fi *
	    cos(this->slip_rad)) /
	    this->inertia;
	this->slip_speed += this->slip_acc * dt;
	this->slip_rad += this->slip_speed * dt;

#if 0
	std::cout << t <<
	    " " << this->l_speed <<
	    " " << this->l_acc <<
	    " " << this->w_speed <<
	    " " << this->w_acc <<
	    " " << this->fi <<
	    " " << this->inertia <<
	    " " << this->angle <<
	    " " << (this->slip_rad * 180.0 / M_PI) <<
	    " " << this->slip_speed <<
	    " " << this->slip_acc <<
	    " " << this->cur_brick_idx <<
	    " " << this->cur_brick_pos <<
	    " " << this->throttle <<
	    " " << this->e_power <<
	    " " << this->drag <<
	    std::endl;
#endif
}
