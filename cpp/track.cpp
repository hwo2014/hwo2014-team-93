/**
 * \file track.cpp
 * \brief Track management.
 *
 * \author Matteo Foppiano <matteo@redsplat.org>
 * \date 17 Apr 2014
 */

#include <rs/track.h>
#include <cassert>
#include <cmath>
#include <iostream>


/************************************************************************* Track
 */

/***************************************************************************//**
 * \brief Constructor.
 *
 */

track::track(void)
	:
	id(""),
	name(""),
	lane_idx(),
	lane_dst(),
	brick()
{
}

/***************************************************************************//**
 * \brief Calculate the length of every lane.
 *
 */

void track::build_lanes_length(void)
{
	for (std::vector<track_piece>::iterator bi = this->brick.begin();
	    bi != this->brick.end();
	    ++bi) {
		for (std::map<int, lane>::iterator li = this->lane_idx.begin();
		    li != this->lane_idx.end();
		    ++li) {
			double len = 0.0;
			if (bi->angle == 0.0) {
				len = bi->length;
			} else if (bi->angle > 0.0) {
				len = bi->angle * M_PI *
				    (bi->radius - li->second.distance) / 180.0;
			} else {
				len = -bi->angle * M_PI *
				    (bi->radius + li->second.distance) / 180.0;
			}
			bi->lane_length.insert(std::pair<double, double>
			    (li->second.distance, len));
		}
	}
}

/***************************************************************************//**
 * \brief Calculate the length of every switch.
 *        (requires length of lanes)
 */

void track::build_switches_length(void)
{
	for (std::vector<track_piece>::iterator bi = this->brick.begin();
	    bi != this->brick.end();
	    ++bi) {
		if (!bi->sw_lane) {
			continue;
		}

		const lane* prev_lane = NULL;
		for (std::map<double, const lane*>::iterator li =
		    this->lane_dst.begin(); li != this->lane_dst.end();
		    ++li) {
			const lane* const cur_lane = li->second;
			if (prev_lane != NULL) {
				double len = sqrt(pow(
				    prev_lane->distance - cur_lane->distance,
				    2.0) +
				    pow((bi->lane_length[prev_lane->distance] +
				    bi->lane_length[cur_lane->distance])/2.0,
				    2.0));
				bi->lane_length.insert(
				    std::pair<double, double>(
				    (prev_lane->distance+cur_lane->distance) /
				    2.0, len));
			}
			prev_lane = cur_lane;
		}
	}
}

/***************************************************************************//**
 * \brief Get precalculated shortest path from a certain lane.
 *
 * \param b_idx brick index of the current position.
 * \param l_idx starting lane index.
 * \param laps number of remaining laps 
 * \param path stores the calculated data.
 * \return shortest path expressed as list of switches.
 *
 */

const path_length& track::get_shortest_path(
    int b_idx, int l_idx, int laps, path_lane& path) const
{
	int l_dst_n = lane_idx2dst2n(l_idx);
	path.ending_lane[l_dst_n].tot_length = 0.0;

	for (int i=0,b=b_idx; i<laps; ++i,b=0) {
		calc_shortest_path(b, path);
	}

	const path_length* best_path = &path.ending_lane[0];
	for (const path_length& el: path.ending_lane) {
		if (el.tot_length < best_path->tot_length &&
		    el.tot_length >= 0.0) {
			best_path = &el;
		}
	}
#if 1
	for (size_t i=0; i<best_path->sw_list.size(); i++) {
		std::cout << "(" << best_path->sw_list.at(i).type <<
		    "," << best_path->sw_list.at(i).b_idx <<
		    "," << best_path->sw_list.at(i).l_idx << ") ";
	}
	if (best_path->sw_list.size() > 0) {
		std::cout << std::endl;
	}
#endif

	return (*best_path);
}

/***************************************************************************//**
 * \brief Calculate the shortest path starting from a certain <brick, lane>.
 *
 * \param start_b_idx brick index to start from.
 * \param pl path for lane to start from.
 *
 */

void track::calc_shortest_path(int start_b_idx, path_lane& pl) const
{
	for (std::vector<track_piece>::const_iterator bi = this->brick.cbegin();
	    bi != this->brick.cend(); ++bi) {
		int b_idx = std::distance(this->brick.cbegin(), bi);

		if (b_idx < start_b_idx) {
			continue;
		}

		for (auto& el: pl.ending_lane) {
			el.new_length = -1.0;
		}
		const lane* prev_lane = NULL;

		for (std::map<double, const lane*>::const_iterator li =
		    this->lane_dst.cbegin(); li != this->lane_dst.cend();
		    ++li) {

			const lane* const cur_lane = li->second;
			size_t i = std::distance(this->lane_dst.begin(), li);

			if (pl.ending_lane[i].tot_length >= 0.0) {
				assert(bi->lane_length.find(cur_lane->distance)
				    != bi->lane_length.end());
				double len = bi->lane_length.find(
				    cur_lane->distance)->second;
				update_short_path(pl.ending_lane[i], len);
			}

			if (bi->sw_lane && li != this->lane_dst.begin()) {
				assert(i > 0);
				assert(prev_lane != NULL);
				assert(bi->lane_length.find(
				    (cur_lane->distance + prev_lane->distance) /
				    2.0) != bi->lane_length.end());

				double len = bi->lane_length.find(
				    (cur_lane->distance + prev_lane->distance) /
				    2.0)->second;
				if (pl.ending_lane[i].tot_length >= 0.0) {
					update_switch_short_path(
					    pl.ending_lane[i-1],
					    pl.ending_lane[i],
					    len, switch_info::SW_LEFT, b_idx,
					    prev_lane->index);
				}
				if (pl.ending_lane[i-1].tot_length >= 0.0) {
					update_switch_short_path(
					    pl.ending_lane[i],
					    pl.ending_lane[i-1],
					    len,
					    switch_info::SW_RIGHT, b_idx,
					    cur_lane->index);
				}
			}
			prev_lane = cur_lane;
		}

		for (auto& el: pl.ending_lane) {
			el.tot_length = el.new_length;
		}

	}
}

/***************************************************************************//**
 * \brief Update the path if the new straight path is shorter.
 *
 * \param dst destination path to update.
 * \param len length of the segment.
 *
 */

void track::update_short_path(path_length& dst, double len) const
{
	if (dst.new_length < 0.0 ||
	    (dst.new_length - (dst.tot_length+len) > PATH_APPROX)) {
		dst.new_length = dst.tot_length + len;
	}
}

/***************************************************************************//**
 * \brief Update the path if the new switch path is shorter.
 *
 * \param dst destination path to update.
 * \param src source path.
 * \param len length of the segment.
 * \param sw_s switch status.
 *
 */

void track::update_switch_short_path(
    path_length& dst, const path_length& src, double len,
    switch_info::sw_type type, int b_idx, int l_idx) const
{
	if (dst.new_length < 0.0 ||
	    (dst.new_length - (src.tot_length+len) > PATH_APPROX)) {
		dst.sw_list = src.sw_list;
		dst.sw_list.push_back(switch_info(type, b_idx, l_idx));
		dst.new_length = src.tot_length + len;
	}
}

/***************************************************************************//**
 * \brief Get the brick index of the next switch.
 *
 * \param b_idx current brick.
 * \return brick index of the next switch or a zero if there aren't switches.
 *
 */

int track::get_switch_after(int b_idx) const
{
	int next_idx = 0;
	for (std::vector<track_piece>::const_iterator bi = this->brick.cbegin();
	    bi != this->brick.cend(); ++bi) {
		int i = std::distance(this->brick.cbegin(), bi);
		if (i <= b_idx) {
			continue;
		}

		if (bi->sw_lane) {
			next_idx = i;
			break;
		}
	}

	return (next_idx);
}

/***************************************************************************//**
 * \brief Get the switch brick index before a specific brick
 *
 * \param b_idx index of the target brick.
 * \return index of the brick of the previous switch or 0 if there aren't
 *         switches.
 *
 */

int track::get_switch_before(int b_idx) const
{
	int prev_idx = 0;
	for (std::vector<track_piece>::const_iterator bi = this->brick.cbegin();
	    bi != this->brick.cend(); ++bi) {
		int i = std::distance(this->brick.cbegin(), bi);
		if (i >= b_idx) {
			break;
		}

		if (bi->sw_lane) {
			prev_idx = i;
		}
	}

	return (prev_idx);
}

/***************************************************************************//**
 * \brief Get the length of a brick for a given lane.
 *
 * \param b_idx index of the brick
 * \param sl_idx index of the starting lane.
 * \param el_idx index of the ending lane.
 * \return length of the brick.
 *
 */

double track::get_lane_length(int b_idx, int sl_idx, int el_idx) const
{
	assert(this->lane_idx.find(sl_idx) != this->lane_idx.end());
	assert(this->lane_idx.find(el_idx) != this->lane_idx.end());
	if (this->lane_idx.find(sl_idx) == this->lane_idx.end() ||
	    this->lane_idx.find(el_idx) == this->lane_idx.end()) {
		return (0.0);
	}

	double sl_dst = this->lane_idx.find(sl_idx)->second.distance;
	double el_dst = this->lane_idx.find(el_idx)->second.distance;
	//std::cout << "f:" << sl_idx << "," << sl_dst << " t:" << el_idx << "," << el_dst << std::endl;
	return (this->brick.at(b_idx)
	    .lane_length.find((sl_dst + el_dst) / 2.0)->second);
}

/***************************************************************************//**
 * \brief Update track statistics using car's data.
 *
 * \param b_idx brick index.
 * \param b_pos position along the current brick.
 * \param l_idx lane index.
 * \param th current throttle.
 * \param deg current car's angle.
 *
 */

void track::update_stats(
    int b_idx, double b_pos, int l_idx, double th, double deg)
{
	const int b_pos_int = static_cast<int>(b_pos + 0.5);
	brick_stats& s = this->brick[b_idx].stats[l_idx][b_pos_int];
#if 0
	if (this->brick[b_idx].stats[l_idx].find(b_pos_int) !=
	    this->brick[b_idx].stats[l_idx].end()) {
	}
#endif

	s.throttle = th;
	s.angle = deg;
	s.is_safe = true;
}

/***************************************************************************//**
 * \brief Set the track ID.
 *
 * \param i track ID.
 *
 */

void track::set_id(const std::string& i)
{
	assert(this->id == "");
	this->id = i;
}

/***************************************************************************//**
 * \brief Set the track name.
 *
 * \param n track name.
 *
 */

void track::set_name(const std::string& n)
{
	assert(this->name == "");
	this->name = n;
}

/***************************************************************************//**
 * \brief Add a lane to the list.
 *
 * \param l lane to add.
 *
 */

void track::add_lane(const lane& l)
{
	this->lane_idx.insert(std::pair<int, lane>(l.index, l));
	this->lane_dst.insert(std::pair<double, const lane*>
	    (l.distance, &this->lane_idx.at(l.index)));
}

/***************************************************************************//**
 * \brief Add a brick to the track pieces list.
 *
 * \param tp brick to add.
 *
 */

void track::add_brick(const track_piece& tp)
{
	this->brick.push_back(tp);
}
