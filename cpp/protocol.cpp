#include <rs/protocol.h>


jsoncons::json hwo_protocol::make_request(
    const std::string& msg_type, const jsoncons::json& data)
{
	jsoncons::json r;
	r["msgType"] = msg_type;
	r["data"] = data;
	return r;
}

jsoncons::json hwo_protocol::make_join(
    const std::string& name, const std::string& key)
{
	jsoncons::json data;
	data["name"] = name;
	data["key"] = key;
	return make_request("join", data);
}

jsoncons::json hwo_protocol::make_joinrace(
    const std::string& name, const std::string& key,
    const std::string& track, const std::string& password,
    int n_players)
{
	jsoncons::json data;
	jsoncons::json bot_id;
	data["botId"] = bot_id;
	data["botId"]["name"] = name;
	data["botId"]["key"] = key;
	data["trackName"] = track;
	if (password != "") {
		data["password"] = password;
	}
	data["carCount"] = n_players;

	return make_request("joinRace", data);
}

jsoncons::json hwo_protocol::make_ping(void)
{
	return make_request("ping", jsoncons::null_type());
}

jsoncons::json hwo_protocol::make_throttle(double throttle)
{
	return make_request("throttle", throttle);
}

jsoncons::json hwo_protocol::make_switch(const switch_info::sw_type type)
{
	if (type != switch_info::SW_LEFT && type != switch_info::SW_RIGHT) {
		return (make_ping());
	}
	return make_request("switchLane",
	    type == switch_info::SW_LEFT ? "Left" : "Right");
}
