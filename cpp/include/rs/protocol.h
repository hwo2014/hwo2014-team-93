#ifndef HWO_PROTOCOL_H
#define HWO_PROTOCOL_H

#include <string>
#include <iostream>
#include <rs/json_wrapper.h>

#include <rs/track.h>


namespace hwo_protocol
{
	jsoncons::json make_request(
	    const std::string& msg_type, const jsoncons::json& data);
	jsoncons::json make_join(
	    const std::string& name, const std::string& key);
	jsoncons::json make_joinrace(
	    const std::string& name, const std::string& key,
	    const std::string& track, const std::string& password,
	    int n_players);
	jsoncons::json make_ping(void);
	jsoncons::json make_throttle(double throttle);
	jsoncons::json make_switch(const switch_info::sw_type type);
}

#endif
