#ifndef __JSON_WRAPPER_H__
#define __JSON_WRAPPER_H__

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Weffc++"
#endif
#include <jsoncons/json.hpp>
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

#endif /* __JSON_WRAPPER_H__ */
