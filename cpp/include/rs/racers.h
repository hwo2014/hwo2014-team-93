/**
 * \file racers.h
 * \brief Racers namespace.
 *
 * \author Matteo Foppiano <matteo@redsplat.org>
 * \date 18 Apr 2014
 */

#ifndef __RACERS_H__
#define __RACERS_H__

#include <iostream>
#include <string>
#include <map>


class car
{
public:
	car(const std::string& n, const std::string& c,
	    double l, double w, double gf);

	inline void update_angle(double a);
	inline void update_brick(int idx, double pos, double rad, double l_len);
	inline void update_lane(int cur, int next);
	inline void update_lap(int l);

	void update_model(int t);
	void update_status(int t);

	inline void set_throttle(double t);

	inline const std::string& get_color(void) const;
	inline double get_angle(void) const;
	inline int get_cur_brick_idx(void) const;
	inline double get_cur_brick_pos(void) const;
	inline int get_cur_lane(void) const;
	inline int get_next_lane(void) const;
	inline int get_lap(void) const;
	inline double get_throttle(void) const;

	inline int get_prev_brick_idx(void) const;
	inline double get_prev_brick_pos(void) const;

	enum fsm_status {
		CAR_S_RACING,
		CAR_S_WAITING,
	};
	inline void set_status(fsm_status s);
	inline fsm_status get_status(void) const;

	static const int UNDEF_LANE = -1;
	static constexpr double MIN_THROTTLE = 0.0;
	static constexpr double MAX_THROTTLE = 1.0;

private:
	const std::string name;
	const std::string color;
	const double length;
	const double width;
	const double guide_flag;
	const double inertia;

	double angle;
	int cur_brick_idx;
	double cur_brick_pos;
	double cur_brick_rad;
	int cur_lane;
	int next_lane;
	double cur_lane_len;
	int lap;
	double throttle;

	int prev_brick_idx;
	double prev_brick_pos;
	double prev_brick_rad;
	double prev_lane_len;

	double l_speed;
	double w_speed;
	double l_acc;
	double w_acc;
	double fi;
	double slip_rad;
	double slip_speed;
	double slip_acc;

	int gametick;

	enum fsm_physic {
		CAR_E_PHYSIC,
		CAR_E_DONE,
	};
	fsm_physic physic_fsm;
	fsm_status state_fsm;
	double e_power;
	double drag;
};

class racers
{
public:
	racers(void);

	void add_car(const car& c);
	bool is_a_racer(const std::string& color) const;
	car& get_racer(const std::string& color);

	void start_all_engines(void);

	inline void clear(void);

private:
	std::map<std::string, car> racer;
};


/***************************************************************************//**
 * \brief Update car angle information.
 *
 * \param a new car angle.
 *
 */

inline void car::update_angle(double a)
{
	this->angle = a;
}

/***************************************************************************//**
 * \brief Update car position information.
 *
 * \param idx number of the brick the car is on (0-index).
 * \param pos position of the guid flag on the current brick.
 * \param rad radius of the current brick.
 * \param l_len length of the current lane.
 *
 */

inline void car::update_brick(int idx, double pos, double rad, double l_len)
{
	this->prev_brick_idx = this->cur_brick_idx;
	this->prev_brick_pos = this->cur_brick_pos;
	this->prev_brick_rad = this->cur_brick_rad;
	this->prev_lane_len = this->cur_lane_len;
	this->cur_brick_idx = idx;
	this->cur_brick_pos = pos;
	this->cur_brick_rad = rad;
	this->cur_lane_len = l_len;
}

/***************************************************************************//**
 * \brief Update lane information.
 *
 * \param cur current lane of the car.
 * \param next next lane of the car.
 *
 */

inline void car::update_lane(int cur, int next)
{
	this->cur_lane = cur;
	this->next_lane = next;
}

/***************************************************************************//**
 * \brief Update lap information.
 *
 * \param l number of laps the car has completed.
 *
 */

inline void car::update_lap(int l)
{
	this->lap = l;
}

/***************************************************************************//**
 * \brief Set throttle value.
 *
 * \param t throttle.
 *
 */

inline void car::set_throttle(double t)
{
	if (t < MIN_THROTTLE) {
		t = MIN_THROTTLE;
	} else if (t > MAX_THROTTLE) {
		t = MAX_THROTTLE;
	}
	this->throttle = t;
}

/***************************************************************************//**
 * \brief Get the car color.
 *
 * \return car color string.
 *
 */

inline const std::string& car::get_color(void) const
{
	return (this->color);
}

/***************************************************************************//**
 * \brief Get the angle of the car.
 *
 * \return angle of the car.
 *
 */

inline double car::get_angle(void) const
{
	return (this->angle);
}

/***************************************************************************//**
 * \brief Get car brick index.
 *
 * \return index of the brick the car is on.
 *
 */

inline int car::get_cur_brick_idx(void) const
{
	return (this->cur_brick_idx);
}

/***************************************************************************//**
 * \brief Get the position of the car on the current brick.
 *
 * \return position of the car on the current brick.
 *
 */

inline double car::get_cur_brick_pos(void) const
{
	return (this->cur_brick_pos);
}

/***************************************************************************//**
 * \brief Get current car lane.
 *
 * \return current car lane.
 *
 */

inline int car::get_cur_lane(void) const
{
	return (this->cur_lane);
}

/***************************************************************************//**
 * \brief Get next car lane.
 *
 * \return next car lane.
 *
 */

inline int car::get_next_lane(void) const
{
	return (this->next_lane);
}

/***************************************************************************//**
 * \brief Get the current lap of the car.
 *
 * \return lap of the car.
 *
 */

inline int car::get_lap(void) const
{
	return (this->lap);
}

/***************************************************************************//**
 * \brief Get car's throttle.
 *
 * \return car's throttle.
 *
 */

inline double car::get_throttle(void) const
{
	return (this->throttle);
}

/***************************************************************************//**
 * \brief Set car FSM status.
 *
 * \param s new FSM status.
 *
 */

inline void car::set_status(fsm_status s)
{
	this->state_fsm = s;
}

/***************************************************************************//**
 * \brief Get car FSM status.
 *
 * \return car FSM status.
 *
 */

inline car::fsm_status car::get_status(void) const
{
	return (this->state_fsm);
}


/***************************************************************************//**
 * \brief Clean all cars items.
 *
 */

inline void racers::clear(void)
{
	this->racer.clear();
}

#endif /* __RACERS_H__ */
