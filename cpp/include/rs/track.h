/**
 * \file track.h
 * \brief Track class definition.
 *
 * \author Matteo Foppiano <matteo@redsplat.org>
 * \date 17 Apr 2014
 */

#ifndef __TRACK_H__
#define __TRACK_H__

#include <cassert>
#include <string>
#include <vector>
#include <array>
#include <map>
#include <queue>


struct lane
{
	lane(int idx, double dist)
		:
		index(idx),
		distance(dist)
	{
	}

	const int index;
	const double distance;

	static const int MAX_N_LANES = 4;
};

struct brick_stats
{
	brick_stats(void)
		:
		throttle(1),
		angle(0),
		is_safe(true)
	{
	}

	double throttle;
	double angle;
	bool is_safe;
};

struct track_piece
{
	track_piece(double len, double r, double ang, bool sw)
		:
		length(len),
		radius(r),
		angle(ang),
		sw_lane(sw),
		lane_length(),
		stats()
	{
	}

	const double length;
	const double radius;
	const double angle;

	const bool sw_lane;

	/*
	 * the index key can be:
	 *   - 'distance' parameter of a lane, the value of the map represents
	 *     the real length of that lane;
	 *   - the average of 'distance' parameters between two adjacent lanes,
	 *     the value of the map represents the length of the switch between
	 *     the two lanes (assuming that going from 1st lane to 2nd lane is
	 *     equal to going from 2nd lane to 1st lane).
	 */
	std::map<double, double> lane_length;

	std::array<std::map<int, brick_stats>, lane::MAX_N_LANES> stats;
};

struct switch_info
{
	enum sw_type
	{
		SW_LEFT = -1,
		SW_AHEAD,
		SW_RIGHT,
	};

	switch_info(sw_type t, int bidx, int lidx)
		:
		type(t),
		b_idx(bidx),
		l_idx(lidx)
	{
	}

	/*const*/ sw_type type;
	/*const*/ int b_idx;
	/*const*/ int l_idx;
};

struct path_length
{
	path_length(void)
		:
		tot_length(-1.0),
		new_length(-1.0),
		sw_list()
	{
	}

	double tot_length;
	double new_length;
	std::vector<switch_info> sw_list;

	/* do not copy the object */
	path_length& operator=(const path_length&);
	path_length(const path_length&);
};

struct path_lane
{
	path_lane(void)
		:
		ending_lane()
	{
	}

	std::array<path_length, lane::MAX_N_LANES> ending_lane;
	/*
	 * ending_lane[x] => shortest path to go from starting lane to ending
	 *                   lane x
	 *                   (where x is the index of the lane in order of
	 *                   distance).
	 */

	/* do not copy the object */
	path_lane& operator=(const path_lane&);
	path_lane(const path_lane&);
};

class track
{
public:
	track(void);

	inline void build_track_lengths(void);
	const path_length& get_shortest_path(
	    int b_idx, int l_idx, int laps, path_lane& path) const;
	int get_switch_after(int b_idx) const;
	int get_switch_before(int b_idx) const;
	double get_lane_length(int b_idx, int sl_idx, int el_idx) const;
	void update_stats(
	    int b_idx, double b_pos, int l_idx, double th, double deg);
	inline int get_next_brick_idx(int b_idx) const;

	void set_id(const std::string& i);
	void set_name(const std::string& n);
	void add_lane(const lane& l);
	void add_brick(const track_piece& tp);
	
	inline double get_brick_radius(int b_idx) const;
	inline double get_brick_angle(int b_idx) const;

	inline void clear(void);

private:
	std::string id;
	std::string name;

	std::map<int, lane> lane_idx;
	std::map<double, const lane*> lane_dst;
	std::vector<track_piece> brick;

	void build_lanes_length(void);
	void build_switches_length(void);
	void calc_shortest_path(int start_b_idx, path_lane& pl) const;
	void update_short_path(path_length& dst, double len) const;
	void update_switch_short_path(
	    path_length& dst, const path_length& src, double len,
	    switch_info::sw_type type, int b_idx, int l_idx) const;

	inline int lane_dst2n(double l_dst) const;
	inline int lane_idx2dst2n(int l_idx) const;

	constexpr static double PATH_APPROX = 0.001;
};


/***************************************************************************//**
 * \brief Calculate the length of every lane and every switch.
 *
 */

inline void track::build_track_lengths(void)
{
	build_lanes_length();
	build_switches_length();
}

/***************************************************************************//**
 * \brief Get the next brick index (looping).
 *
 * \param b_idx current brick index.
 * \return next brick index.
 *
 */

inline int track::get_next_brick_idx(int b_idx) const
{
	if (b_idx < 0 || b_idx >= static_cast<int>(this->brick.size())) {
		return (0);
	}
	return (b_idx+1);
}

/***************************************************************************//**
 * \brief Get brick radius.
 *
 * \param b_idx index of the brick.
 * \return radius of the brick.
 *
 */

inline double track::get_brick_radius(int b_idx) const
{
	assert(b_idx < static_cast<int>(this->brick.size()));

	double radius = 0.0;
	if (b_idx < static_cast<int>(this->brick.size())) {
		radius = this->brick.at(b_idx).radius;
	}
	return (radius);
}

/***************************************************************************//**
 * \brief Get brick angle.
 *
 * \param b_idx index of the brick.
 * \return angle of the brick.
 *
 */

inline double track::get_brick_angle(int b_idx) const
{
	assert(b_idx < static_cast<int>(this->brick.size()));

	double angle = 0.0;
	if (b_idx < static_cast<int>(this->brick.size())) {
		angle = this->brick.at(b_idx).angle;
	}
	return (angle);
}

/***************************************************************************//**
 * \brief Clean all bricks and lanes items.
 *
 */

inline void track::clear(void)
{
	this->id = "";
	this->name = "";

	this->lane_dst.clear();
	this->lane_idx.clear();
	this->brick.clear();
}

/***************************************************************************//**
 * \brief Get the index of an element in the distance based map.
 *
 * \param l_dst distance of the lane
 * \return index of the item in the distance based map.
 *
 */

inline int track::lane_dst2n(double l_dst) const
{
	return (
	    std::distance(this->lane_dst.begin(), this->lane_dst.find(l_dst)));
}

/***************************************************************************//**
 * \brief Convert lane index number to distance based map index.
 *
 * \param l_idx lane index.
 * \return index of the item in the distance based map.
 *
 */

inline int track::lane_idx2dst2n(int l_idx) const
{
	return (lane_dst2n(this->lane_idx.find(l_idx)->second.distance));
}


#endif /* __TRACK_H__ */
