/**
 * \file engine.h
 * \brief Engine class definition.
 *
 * \author Matteo Foppiano <matteo@redsplat.org>
 * \date 18 Apr 2014
 */

#ifndef __ENGINE_H__
#define __ENGINE_H__

#include <queue>
#include <iostream>
#include <rs/json_wrapper.h>
#include <rs/protocol.h>
#include <rs/track.h>
#include <rs/racers.h>


class engine
{
public:
	enum fsm_eval
	{
		EVAL_ROUTE,
		EVAL_THROTTLE,
	};

	engine(const std::string& name);

	inline void update_gametick(int gt);
	void set_track(const jsoncons::json& j_track);
	void set_cars(const jsoncons::json& j_cars);
	inline void set_tot_laps(int tl);
	inline const jsoncons::json msg_pop(void);
	inline void clear_all(void);

	/* sense */
	void update_cars(const jsoncons::json& j_cars);
	void car_crashed(const jsoncons::json& j_car);
	void car_spawn(const jsoncons::json& j_car);
	void game_start(void);

	/* eval */
	void eval(void);

	/* act */
	void set_switch_lane(switch_info::sw_type dir);
	void set_throttle(double th);

private:
	const std::string my_name;
	int gametick;
	int tot_laps;
	int path_eval_brick;
	fsm_eval main_fsm;

	track racing_trk;
	racers players;
	car* my_car;

	std::queue<jsoncons::json> msg_fifo;

	void evolve_fsm(void);
	void eval_track_route(void);
	void eval_car_throttle(void);

	/* do not copy the object */
	engine& operator=(const engine&);
	engine(const engine&);
};


/***************************************************************************//**
 * \brief Update gametick.
 *
 * \param gt game tick.
 *
 */

inline void engine::update_gametick(int gt)
{
	this->gametick = gt;
}

/***************************************************************************//**
 * \brief Set the number of total laps for this session.
 *
 * \param tl number of total laps.
 *
 */

inline void engine::set_tot_laps(int tl)
{
	this->tot_laps = tl;
}

/***************************************************************************//**
 * \brief Pop a json message from the message FIFO.
 *
 * \return json message ('ping' if FIFO is empty).
 *
 */

inline const jsoncons::json engine::msg_pop(void)
{
	if (this->msg_fifo.empty() || this->gametick < 0) {
		return (hwo_protocol::make_ping());
	}

	jsoncons::json el = this->msg_fifo.front();
	this->msg_fifo.pop();
	el["gameTick"] = this->gametick;
	return (el);
}


/***************************************************************************//**
 * \brief Remove track and cars information.
 *
 */

inline void engine::clear_all(void)
{
	this->racing_trk.clear();
	this->players.clear();
	this->my_car = NULL;
	this->gametick = 0;
	this->tot_laps = 0;
	this->main_fsm = EVAL_ROUTE;
}

#endif /* __ENGINE_H__ */
