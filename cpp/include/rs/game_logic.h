#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <rs/json_wrapper.h>

#include <rs/engine.h>


class game_logic
{
public:
	typedef std::vector<jsoncons::json> msg_vector;

	game_logic(const std::string& name);
	msg_vector react(const jsoncons::json& msg);

private:
	typedef std::function
	    <bool (game_logic*, const jsoncons::json&, bool)> action_fun;
	const std::map<std::string, action_fun> action_map;

	bool on_join(const jsoncons::json& data, bool sync);
	bool on_join_race(const jsoncons::json& data, bool sync);
	bool on_your_car(const jsoncons::json& data, bool sync);
	bool on_game_init(const jsoncons::json& data, bool sync);
	bool on_game_start(const jsoncons::json& data, bool sync);
	bool on_car_positions(const jsoncons::json& data, bool sync);
	bool on_crash(const jsoncons::json& data, bool sync);
	bool on_lap_finished(const jsoncons::json& data, bool sync);
	bool on_finish(const jsoncons::json& data, bool sync);
	bool on_game_end(const jsoncons::json& data, bool sync);
	bool on_tournament_end(const jsoncons::json& data, bool sync);
	bool on_error(const jsoncons::json& data, bool sync);
	bool on_turbo(const jsoncons::json& data, bool sync);
	bool on_spawn(const jsoncons::json& data, bool sync);
	bool on_dnf(const jsoncons::json& data, bool sync);

	engine driveshaft;
};

#endif
